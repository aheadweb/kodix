export const InputComponent = (className) => {
    const baseJsClass = `.js-${className}`;
    const baseFocusClass = `${className}_focus`;
    const nodes = document.querySelectorAll(baseJsClass) || [];
    nodes.forEach(node => {
        const nodeInput = node.querySelector("input");
        if (nodeInput) {
            nodeInput.addEventListener("focus", () => node.classList.add(baseFocusClass));
            nodeInput.addEventListener("blur", onBlurInput.bind(this,node,baseFocusClass));
        }
    });
};


function onBlurInput(node,baseFocusClass,{target}) {
    target.value.length > 1
        ? node.classList.add(baseFocusClass)
        : node.classList.remove(baseFocusClass);
}