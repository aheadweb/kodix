class BaseSelectComponent {
    constructor(node) {
        this.body = node;
        this.baseOpenClass = "select_open";
        this.baseJsInputClass = "js-select-input";
        this.input = this.body.querySelector(`.${this.baseJsInputClass}`);
        this.baseJsPlaceholderClass = "js-select-title";
        this.placeholder = this.body.querySelector(`.${this.baseJsPlaceholderClass}`);
        this.baseJsListCLass = "js-select-list";
        this.init();
    }

    init() {
        if (!this.body) {
            console.error(`Can't init select on ${this.node}! Probably is not htmlElement`);
            return;
        }

        this.bindClickSelect();
        this.bindGlobalClose();
    }

    bindGlobalClose() {
        document.addEventListener("click", (e) => {
            const node = e.target;
            
            if (!node.closest(".js-select")) {
                this.closeSelect();
            }
        });
    }

    bindClickSelect() {
        this.body.addEventListener("click", ({ target }) => {

            if (target.classList.contains(this.baseJsPlaceholderClass)) {
                this.body.classList.contains(this.baseOpenClass)
                    ? this.closeSelect()
                    : this.openSelect();
                return;
            }

            if (target.closest(`.${this.baseJsListCLass}`)) {
                this.onItemClick(target);
                return;
            }
        });
    }

    openSelect() {
        this.body.classList.add(this.baseOpenClass);
    }

    closeSelect() {
        this.body.classList.remove(this.baseOpenClass);
    }

    onItemClick(node) {
        const value = node.dataset.value;
        if (!value) {
            return;
        }

        this.setValueOnInput(value);
        this.setTitleText(node.textContent);
        this.closeSelect();
    }

    setTitleText(text) {
        this.placeholder.textContent = text;
    }

    setValueOnInput(value) {
        this.input.value = value;
    }
}


export default BaseSelectComponent;