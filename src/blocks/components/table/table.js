import iRequest from "../../../js/interfaces/IRequest";

class Table extends iRequest {
    constructor({ body, fetchUrl }) {
        super();
        this.baseJsClass = body;
        this.fetchUrl = fetchUrl;
        this.baseBodyClass = `${this.baseJsClass}-body`;
        this.baseJsDeleteClass = `${this.baseJsClass}-delete-row`;
        this.baseJSRowClass = `${this.baseJsClass}-row`;
        this.table = document.querySelector(this.baseJsClass);
        this.body = this.table.querySelector(this.baseBodyClass);
        this.statusEnum = {
            pednding: "Ожидается",
            out_of_stock: "Нет в наличии",
            in_stock: "В наличии"
        };
    }

    init() {
        if (this.body === null) {
            console.warn(`Can't init table component. Not found ${this.baseJsClass}`);
            return;
        }

        console.info("Table component init");
        this.globalBindDeleteRow();
        this.toggleStatusloading(true);
        this.fetchData();

    }

    globalBindDeleteRow() {
        document.addEventListener("click", ({ target }) => {
            if (target.classList.contains(this.baseJsDeleteClass.slice(1))) {
                target.closest(this.baseJSRowClass).remove();
            }
        });
    }

    toggleStatusloading(flag) {
        const action = flag ? "add" : "remove";
        this.table.classList[action]("table_loading");
    }

    fetchData() {
        super.makeRequest(this.fetchUrl)
            .then(this.renderTable.bind(this));
    }

    renderTable(array) {
        const htmlRows = array
            .map(this.getHtmlRowWithData.bind(this))
            .join("");

        //fake delay                        
        setTimeout(() => {
            this.body.innerHTML = htmlRows;
            this.toggleStatusloading(false);
        }, 2000);
    }

    dispatch(type,payload) {
        if(type === "SET_NEW_ROW") {
            this.toggleStatusloading(true);
            const html = this.getHtmlRowWithData(payload);
            this.body.innerHTML = this.body.innerHTML + html;
            this.toggleStatusloading(false);
        }
    }


    getHtmlRowWithData({ title, id, color, description, price, status, year }) {
        const empryDescrClass = description === "" ? "table__col_empty" : "";
        return `
        <div class="table__row js-table-row" data-id=${id}>
            <div class="table__col">${title}</div>
            <div class="table__col">${year}</div>
            <div class="table__col">
              <div class="color-circle" data-color="${color}"></div>
            </div>
            <div class="table__col">${this.statusEnum[status]}</div>
            <div class="table__col">${price} руб.</div>
            <div class="table__col">
              <button class="btn btn_delete js-table-delete-row">Удалить</button>
            </div>
            <div class="table__col ${empryDescrClass}"><span class="table__col-descr">${description}</span></div>
        </div>
      `;
    }
}

export default Table;