class MainPage {
    constructor({
        body,
        sendEvent
    }) {
        this.body = document.querySelector(body);
        this.form = this.body.querySelector(".js-main-form");
        this.btnSend = this.body.querySelector(".js-main-btn-send");
        this.sendMyEvent = sendEvent;
        this.enumFormFields = {
            title: "Название",
            year: "Год",
            color: "Цвет",
            status: "Статус",
            price: "Цена",
            description: "Описание"
        };
    }

    init() {
        if (this.body === null) {
            console.warn("Can't init mainPage component. Not found main htmlElment");
            return;
        }

        this.btnSend.addEventListener("click", this.sendForm.bind(this));
        console.log("MainPage init");

    }

    sendForm({ target }) {
        let hasError = false;
        const form = target.closest(".js-main-form");
        const formData = new FormData(form);
        const obj = {};
        formData.forEach((value, key) => {
            if (value === "") {
                alert(`Поле '${this.enumFormFields[key]}' пустое/ не выбранное`);
                hasError = true;
            }
            obj[key] = value;
        });

        if (!obj.color) {
            alert(`Поле '${this.enumFormFields["color"]}' пустое/ не выбранное`);
            hasError = true;
        }
        if (!hasError) {
            obj.id = Date.now();
            this.sendMyEvent({
                type: "SET_NEW_ROW",
                payload: obj
            });
            this.resetFields();
        }
    }

    resetFields() {
        this.form.querySelectorAll("input")
            .forEach(input => {
                input.value = "";
            });
    }
}

export default MainPage;