class App {
    constructor({ dependency, components = [], modules = [] }) {
        this.components = components;
        this.modules = modules;
        dependency();
        this.init();
    }

    init() {
        this.components.forEach(component => {
            if (component.init) {
                component.init();
            }
        });

        this.modules.forEach(module => {
            if (module.init) {
                module.init();
            }
        });

        console.info("App init");
    }

    sendEvent({ type, payload }) {
        switch (type) {
        case "SET_NEW_ROW":
            this.components.forEach((component) => {
                if (component.baseJsClass === ".js-table") {
                    component.dispatch(type,payload);
                }
            });
            break;
        }
    }

}

export default App;