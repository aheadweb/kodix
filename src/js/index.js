import { InputComponent } from "../blocks/components/input/input";
import BaseSelectComponent from "../blocks/components/select/select";
import Table from "../blocks/components/table/table";
import MainPage from "../blocks/modules/main/main";
import App from "./App";

const TABLE_FETCH_URL = "https://rawgit.com/Varinetz/e6cbadec972e76a340c41a65fcc2a6b3/raw/90191826a3bac2ff0761040ed1d95c59f14eaf26/frontend_test_table.json";

window.addEventListener("load", () => {
    const app = new App({
        components: [
            new Table({
                body: ".js-table",
                fetchUrl: TABLE_FETCH_URL,
            }),
        ],
        modules: [
            new MainPage({
                body: ".js-main",
                sendEvent: (props) => app.sendEvent(props)
            })
        ],
        dependency: () => {
            InputComponent("field");
            document.querySelectorAll(".js-select").forEach(node => new BaseSelectComponent(node));
        }
    });
});
