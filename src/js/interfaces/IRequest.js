class iRequest {
    makeRequest(url = "", options = {}) {
        return new Promise((resolve, reject) => {
            fetch(url, options)
                .then(res => res.json())
                .then(resolve)
                .catch(reject);
        });
    }
}

export default iRequest;